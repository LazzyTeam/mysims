import pygame
import game
import numpy
import sprites
import colors


class Unit(pygame.sprite.Sprite):

    def __init__(self, image, pos, scale = None, zIndex = 0, blocked=None, hoverable=False, speed=5):
        pygame.sprite.Sprite.__init__(self)
        if scale:
            image = pygame.transform.scale(image, scale)
        self.image = image
        self.rect = self.image.get_rect(center=pos)
        self.target = None
        self.zIndex = zIndex
        self.hover = False
        self.hoverable = hoverable
        self.blocked = blocked
        self.speed = speed
        if self.hoverable:
            self.hoverImage = sprites.colorize(image, colors.LIGHT_BLUE)

    def draw(self):
        if self.hover and self.hoverable:
            game.surface.blit(self.hoverImage, self.rect)
        else:
            game.surface.blit(self.image, self.rect)

    def update(self, blockUnits = []):
        if self.target:
            vector = numpy.subtract(self.target, self.rect.center)
            d = abs(vector[0]) + abs(vector[1])
            if d >= self.speed:
                normal = (vector[0]/d, vector[1]/d)
                leftFoot = (self.rect.left + normal[0]*self.speed + self.rect.width/3, self.rect.top + normal[1]*self.speed + self.rect.height*0.75)
                rightFoot = (self.rect.left + normal[0]*self.speed + 2*self.rect.width/3, self.rect.top + normal[1]*self.speed + self.rect.height*0.75)
                blocked = False
                for unit in blockUnits:
                    if unit.blocked:
                        if unit.rect.collidepoint(leftFoot):
                            pos = (int(leftFoot[0]-unit.rect.left), int(leftFoot[1]-unit.rect.top))
                            blocked = unit.blocked.get_at(pos)[3]
                            if blocked:
                                break
                        if unit.rect.collidepoint(rightFoot):
                            pos = (int(rightFoot[0]-unit.rect.left), int(rightFoot[1]-unit.rect.top))
                            blocked = unit.blocked.get_at(pos)[3]
                            if blocked:
                                break
                if not blocked:
                    self.rect.move_ip(normal[0]*self.speed, normal[1]*self.speed)
                    self.zIndex = self.rect.bottom
                else:
                    self.targetLost()
            else:
                self.targetLost()

    def targetLost(self):
        self.target = None



class ConnectedUnit(Unit):
    def __init__(self, sprite, pos, scale = None, zIndex = 0, blockedMap=None):
        super().__init__(sprite['*'], pos, scale, zIndex)
        self.sprite = sprite
        self.blockedMap = blockedMap
        if blockedMap:
            self.blocked = blockedMap['*']

    def connect(self, neighbours: tuple):
        right, left, top, bottom = neighbours
        if right and left and top and bottom:
            self.image = self.sprite['┼']
            if self.blockedMap:
                self.blocked = self.blockedMap['┼']
        elif right and left and not top and not bottom:
            self.image = self.sprite['─']
            if self.blockedMap:
                self.blocked = self.blockedMap['─']
        elif not right and not left and top and bottom:
            self.image = self.sprite['│']
            if self.blockedMap:
                self.blocked = self.blockedMap['│']
        elif not right and not left and not top and not bottom:
            self.image = self.sprite['*']
            if self.blockedMap:
                self.blocked = self.blockedMap['*']
        elif right and not left and not top and bottom:
            self.image = self.sprite['┌']
            if self.blockedMap:
                self.blocked = self.blockedMap['┌']
        elif right and not left and top and bottom:
            self.image = self.sprite['├']
            if self.blockedMap:
                self.blocked = self.blockedMap['├']
        elif right and left and not top and bottom:
            self.image = self.sprite['┬']
            if self.blockedMap:
                self.blocked = self.blockedMap['┬']
        elif not right and left and not top and not bottom:
            self.image = self.sprite['>']
            if self.blockedMap:
                self.blocked = self.blockedMap['>']
        elif not right and left and top and not bottom:
            self.image = self.sprite['┘']
            if self.blockedMap:
                self.blocked = self.blockedMap['┘']
        elif right and left and top and not bottom:
            self.image = self.sprite['┴']
            if self.blockedMap:
                self.blocked = self.blockedMap['┴']
        elif not right and not left and not top and bottom:
            self.image = self.sprite['^']
            if self.blockedMap:
                self.blocked = self.blockedMap['^']
        elif not right and left and not top and bottom:
            self.image = self.sprite['┐']
            if self.blockedMap:
                self.blocked = self.blockedMap['┐']
        elif not right and left and top and bottom:
            self.image = self.sprite['┤']
            if self.blockedMap:
                self.blocked = self.blockedMap['┤']
        elif not right and not left and top and not bottom:
            self.image = self.sprite['v']
            if self.blockedMap:
                self.blocked = self.blockedMap['v']
        elif right and not left and not top and not bottom:
            self.image = self.sprite['<']
            if self.blockedMap:
                self.blocked = self.blockedMap['<']
        elif right and not left and top and not bottom:
            self.image = self.sprite['└']
            if self.blockedMap:
                self.blocked = self.blockedMap['└']


class AutoOrientedUnit(Unit):
    def __init__(self, sprite, pos, scale = None, zIndex = 0):
        super().__init__(sprite['south'], pos, scale, zIndex)
        self.sprite = sprite
        self.south = pygame.transform.scale(sprite['south'], scale) if scale else sprite['south']
        self.north = pygame.transform.scale(sprite['north'], scale) if scale else sprite['north']
        self.east = pygame.transform.scale(sprite['east'], scale) if scale else sprite['east']
        self.west = pygame.transform.scale(sprite['west'], scale) if scale else sprite['west']

    def update(self, blockableUnits = []):
        super().update(blockableUnits)
        if self.target:
            dX = self.target[0] - self.rect.center[0]
            dY = self.target[1] - self.rect.center[1]
            vertical = abs(dX) < abs(dY)
            if vertical:
                if dY < 0:
                    self.image = self.north
                else:
                    self.image = self.south
            else:
                if dX > 0:
                    self.image = self.west
                else:
                    self.image = self.east

    def targetLost(self):
        super().targetLost()
        self.image = self.south

