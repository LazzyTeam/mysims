from gamescreen import GameScreen


class EmptyScreen(GameScreen):
    def __init__(self):
        super().__init__()
        self.reset()

    def reset(self):
        super().reset()

    def clear(self):
        super().clear()

    def call(self, event):
        super().call(event)

    def draw(self):
        super().draw()
