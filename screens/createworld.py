import pygame
import colors
import gui
import game
import images
from translate import _
import savefile
from gamescreen import GameScreen


class CreateWorld(GameScreen):
    def __init__(self):
        super(CreateWorld, self).__init__()
        self.reset()
        self.offset = 5

        input = gui.TextInput(200, self.offset * 7, self.screenWidth - self.offset * 7 - 200, 30, key='worldName')
        self.inputs.append(input)

        buttonWidth = 120
        buttonHeight = 40
        btn = gui.Button(pygame.Rect(self.screenWidth/2-buttonWidth-2.5, self.screenHeight-100, buttonWidth, buttonHeight), _('Back'),
                         lambda self: game.changeScreen('MainMenu'), icon=images.BUTTON_BACK, key=pygame.K_ESCAPE)
        self.buttons.append(btn)
        btn = gui.Button(pygame.Rect(self.screenWidth/2+2.5, self.screenHeight-100, buttonWidth, buttonHeight), _('Create'),
                         lambda self: self.proceed())
        self.buttons.append(btn)

    def draw(self):
        self.drawBackground()
        super().draw()

    def clear(self):
        game.surface.fill(colors.LIGHT_BLUE)
        super().clear()

    def drawBackground(self):

        render = game.font.render(_("World creation"), False, colors.WHITE, colors.LIGHT_BLUE)
        game.surface.blit(render, (self.screenWidth/2-render.get_width()/2, 1))

        rect = (self.offset*5, self.offset*5, self.screenWidth-self.offset*10, 50)
        pygame.draw.rect(game.surface, colors.WHITE, rect, 2)
        render = game.font.render(_("World name"), False, colors.WHITE)
        game.surface.blit(render, (self.offset*7, self.offset*5+25-render.get_height()/2))

    def proceed(self):
        worldName = False
        for input in self.inputs:
            if input.key == 'worldName':
                worldName = input.text

        if worldName and len(worldName) > 0:
            self.clear()
            render = game.font.render(_("Creating..."), False, colors.WHITE)
            game.surface.blit(render, (self.screenWidth/2-render.get_width()/2, self.screenWidth/2-render.get_height()/2))
            savefile.createNewFile(worldName)
            pygame.time.delay(1000)
            game.changeScreen('MainMenu')

