import pygame
import colors
import gui
import game
import images
from translate import _
from gamescreen import GameScreen

class MainMenu(GameScreen):
    '''
    Main menu
    '''
    def __init__(self):
        super(MainMenu, self).__init__()
        menuItems = (
            (_('[l] Load world'), lambda self: game.changeScreen('SelectWorld'), pygame.K_l),
            (_('[c] Create new world'), lambda self: game.changeScreen('CreateWorld', [], True), pygame.K_c),
            (_('[t] Settings'), lambda self: game.changeScreen('SettingsMenu'), pygame.K_t),
            (_('[x] Exit'), lambda self: exit(0), pygame.K_x),
            (_('T[e]st'), lambda self: game.changeScreen('TestScreen', [], True), pygame.K_e),
        )
        buttonWidtn = 250
        buttonHeight = 40
        buttonOffset = 10
        x = (self.screenWidth - buttonWidtn)/2
        y = (self.screenHeight - (buttonHeight + buttonOffset) * len(menuItems) - 20)
        for name, click, key in menuItems:
            btn = gui.Button(pygame.Rect(x, y, buttonWidtn, buttonHeight), name, click, key=key)
            self.buttons.append(btn)
            y += buttonHeight + buttonOffset

    def draw(self):
        game.surface.blit(images.LOGO, (self.screenWidth/2 - images.LOGO.get_width()/2, 50))
        super().draw()

    def clear(self):
        game.surface.fill(colors.LIGHT_BLUE)
